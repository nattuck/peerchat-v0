# Peerchat

A peer to peer group chat program.

Each group forms a separate P2P network, protected and authenticated by a secret
passphrase.

Anyone can send a message and it will be propagated to all connected hosts.


