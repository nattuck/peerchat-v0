
use sodiumoxide::crypto::sign::PublicKey;

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct User {
    pub name: String,
    pub pubk: PublicKey,
    pub last: u64,
}

impl User {
    pub fn new(name: String, pubk: PublicKey, last: u64) -> Self {
        User{name, pubk, last}
    }
}

