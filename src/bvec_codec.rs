
use std::net::IpAddr;

use tokio::prelude::*;
use tokio::net::TcpStream;
use tokio::io;
use bytes::{BytesMut, BufMut, Buf};
use std::io::Cursor;

// This sends and recieves messages formatted as (usize, [u8]) pairs.
pub struct BvecCodec {
    sock: TcpStream,
    ibuf: BytesMut,
    obuf: BytesMut,
}

impl BvecCodec {
    pub fn new(sock: TcpStream) -> Self {
        BvecCodec{
            sock,
            ibuf: BytesMut::new(),
            obuf: BytesMut::new(),
        }
    }

    pub fn remote_ip(&self) -> IpAddr {
        self.sock.peer_addr().unwrap().ip()
    }

    fn msg_size(&self) -> Option<usize> {
        if self.ibuf.len() < 8 {
            None
        }
        else {
            let bs = self.ibuf.windows(8).nth(0).unwrap();
            Some(Cursor::new(bs).get_u64_be() as usize)
        }
    }

    fn fill_read_buf(&mut self) -> Poll<(), io::Error> {
        loop {
            self.ibuf.reserve(8);
            let nn = try_ready!(self.sock.read_buf(&mut self.ibuf));

            if nn == 0 {
                // Signal socket closed.
                return Ok(Async::Ready(()));
            }
            else {
                if let Some(size) = self.msg_size() {
                    self.ibuf.reserve(size);
                }
            }
        }
    }
}

impl Stream for BvecCodec {
    type Item = Vec<u8>;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        let closed = self.fill_read_buf()?.is_ready();

        if let Some(size) = self.msg_size() {
            if self.ibuf.len() >= 8 + size {
                let _sz = self.ibuf.split_to(8);
                let msg = self.ibuf.split_to(size);
                return Ok(Async::Ready(Some(msg.to_vec())));
            }
        }

        if closed {
            Ok(Async::Ready(None))
        }
        else {
            Ok(Async::NotReady)
        }
    }
}

impl Sink for BvecCodec {
    type SinkItem = Vec<u8>;
    type SinkError = io::Error;

    fn start_send(&mut self, item: Self::SinkItem)
      -> Result<AsyncSink<Self::SinkItem>, Self::SinkError>
    {
        let size = item.len();
        self.obuf.reserve(8 + size);
        self.obuf.put_u64_be(size as u64);
        self.obuf.put(item);
        Ok(AsyncSink::Ready)
    }

    fn poll_complete(&mut self) -> Poll<(), Self::SinkError>
    {
        while !self.obuf.is_empty() {
            let nn = try_ready!(self.sock.poll_write(&self.obuf));
            assert!(nn > 0);
            let _ = self.obuf.split_to(nn);
        }

        Ok(Async::Ready(()))
    }
}

