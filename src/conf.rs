
use std::fs::{read, write};

use sodiumoxide::crypto::sign::{gen_keypair, PublicKey, SecretKey};
use sodiumoxide::crypto::secretbox;
use sodiumoxide::crypto::pwhash;
use xdg::BaseDirectories;
use toml;

use ::Result;
use args::Args;

#[derive(Serialize, Deserialize, Debug)]
pub struct Conf {
    pub user: String,
    pub pubk: PublicKey,
    pub seck: SecretKey,
    pub pass: String,
    pub boxk: secretbox::Key,
    pub port: u16,
}

fn pass_to_key(pass: String) -> secretbox::Key {
    let mut kbytes = [0; secretbox::KEYBYTES];
    let zero_salt = pwhash::Salt::from_slice(&[0; 32]).unwrap();
    pwhash::derive_key(
        &mut kbytes,
        pass.as_bytes(),
        &zero_salt,
        pwhash::OPSLIMIT_INTERACTIVE,
        pwhash::MEMLIMIT_INTERACTIVE).unwrap();
    secretbox::Key::from_slice(&kbytes).unwrap()
}

impl Conf {
    pub fn load(args: Args) -> Result<Self> {
        let xdg = BaseDirectories::with_profile("peerchat", &args.user)?;
        if let Some(path) = xdg.find_config_file("config") {
            let data = read(&path)?;
            let mut conf: Conf = toml::from_slice(&data)?;
            if let Some(port) = args.port {
                conf.port = port;
            }
            if let Some(pass) = args.pass {
                conf.pass = pass;
            }
            write(path, toml::to_vec(&conf)?)?;
            Ok(conf)
        }
        else {
            let path = xdg.place_config_file("config")?;
            let (pubk, seck) = gen_keypair();
            let pass = args.pass.expect("Must specify password");

            let conf = Conf{
                user: args.user,
                pubk: pubk,
                seck: seck,
                pass: pass.clone(),
                boxk: pass_to_key(pass),
                port: args.port.expect("Must specify listen port"),
            };
            write(path, toml::to_vec(&conf)?)?;
            Ok(conf)
        }
    }
}

