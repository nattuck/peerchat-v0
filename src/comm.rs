
use std;
use std::error::Error;
use std::net::{IpAddr, SocketAddr};

use tokio::net::{TcpListener, TcpStream};
use tokio::prelude::*;
use tokio;

use futures::sync::mpsc;
use futures;

use errors::Result;
use msg::{Msg, ChatBody};
use msg_codec::MsgCodec;
use peer::Peer;
use host::Host;
use state;
use term;

fn link_up(conn: MsgCodec, addr: SocketAddr) {
    let (tx, rx) = mpsc::channel(10);
    let peer = Peer::new(format!("{:?}", &addr), tx);
    state::put_peer(peer);

    let (tx_conn, rx_conn) = conn.split();

    let rx_link = rx_conn.for_each(|msg| {
        got_message(msg).unwrap();
        Ok(())
    }).and_then(|_| {
        println!("peer disconnected");
        //rx.close();
        Ok(())
    }).map_err(|ee| {
        println!("error: {}", ee.description());
    });

    tokio::spawn(rx_link);

    let tx_link = rx.map_err(|ee| {
        std::io::Error::new(std::io::ErrorKind::Other, "none")
    }).forward(tx_conn)
        .and_then(|_| Ok(()))
        .map_err(|_| {});

    tokio::spawn(tx_link);
}

fn handshake(sock: TcpStream) -> impl Future<Item=(MsgCodec, u16), Error=()> {
    let conn = MsgCodec::new(sock);

    let conf = state::get_conf();
    let hello = Msg::Hello{
        vers: 0,
        port: conf.port,
    };

    conn.send(hello).map_err(|_| ()).and_then(|conn| {
        conn.into_future().then(|data| {
            match data {
                Ok((Some(Msg::Hello{ vers, port }), rest)) => {
                    term::putln(&format!("got hello: v{} :{}", vers, port));
                    Ok((rest, port))
                },
                Ok((bad, _rest)) => {
                    println!("Expected hello, got: {:?}", bad);
                    Err(())
                },
                Err((ee, _rest)) => {
                    println!("Expected hello, err: {:?}", ee.description());
                    Err(())
                }
            }
        })
    })
}

fn broadcast(msg: Msg) -> Result<()> {
    let peers_arc = state::get_peers();
    let mut peers = peers_arc.lock().unwrap();
    for pp in &mut *peers {
        if pp.alive() {
            pp.send(msg.clone());
        }
    }

    Ok(())
}

pub fn got_message(msg: Msg) -> Result<()> {
    match msg.clone() {
        Msg::Chat{ sbod: _, pubk } => {
            let body = ChatBody::verify(&msg).unwrap();
            state::put_user(&body.user, &pubk)?;

            let last = state::update_user_last(&body.user, body.time)?;
            if last >= body.time {
                return Ok(());
            }

            let line = format!("{}: {}", body.user, body.text);
            term::putln(&line);

            broadcast(msg)?;
        },
        Msg::Host{ host } => {
            println!("incoming Host: {:?}", &host);
            let host_addr = host.get_sock_addr();
            if None == state::find_host(host_addr) {
                println!("rebroadcast Host: {:?}", &host);
                state::put_host(host);
                broadcast(msg)?;
            }
        },
        msg => {
            return Err(format!("{:?}", msg).into());
        }
    }

    Ok(())
}

fn add_host(addr: IpAddr, port: u16) -> Result<()> {
    let host = Host::new(addr, port, 0);
    println!("New host, should broadcast: {:?}", &host);
    let msg = Msg::Host{ host: host.clone() };
    got_message(msg).map_err(|ee| {
        println!("error: {:?}", ee);
        ee
    })
}

fn probe_peer(addr: SocketAddr) {
    let probe = TcpStream::connect(&addr).and_then(move |sock| {
        let txt = format!("Connected to {} for probe", addr);
        term::putln(txt);

        tokio::spawn(handshake(sock).and_then(|(conn, port)| {
            add_host(conn.remote_ip(), port);
            Ok(())
        }));

        Ok(())
    }).map_err(|_| {});

    tokio::spawn(probe);
}

fn listen(port: u16) -> impl Future<Item=(), Error=()> + Send {
    let addr = format!("127.0.0.1:{}", port).parse().unwrap();
    println!("listen {:?}", addr);

    let tcp = TcpListener::bind(&addr).unwrap();
    let server = tcp.incoming().for_each(move |sock| {
        let txt = format!("Connection from {}", addr);
        term::putln(txt);

        tokio::spawn(handshake(sock).and_then(|(conn, port)| {
            // They connected to us. We should check if they
            // accept incoming connections.

            // If they aren't known already, connect to
            // them for just a handshake. If that works, add
            // them to the host list and broadcast them.
            let peer_addr = SocketAddr::new(conn.remote_ip(), port);
            if None == state::find_host(peer_addr) {
                probe_peer(peer_addr);
            }

            link_up(conn, peer_addr);
            Ok(())
        }));

        Ok(())
    }).map_err(|err| {
        println!("error: {:?}", err);
    });

    Box::new(server)
}

fn connect(port: u16) -> impl Future<Item=(), Error=()> + Send {
    let addr = format!("127.0.0.1:{}", port).parse().unwrap();

    TcpStream::connect(&addr).and_then(move |sock| {
        let txt = format!("Connected to {}", addr);
        term::putln(txt);

        tokio::spawn(handshake(sock).and_then(|(conn, port)| {

            // We connect to them, so we know they can accept
            // incoming connections.

            // TODO: If they aren't known already, add them
            // to the host list and broadcast them.

            // Better - always broadcast on connect. No need to save
            // bandwidth here - N is small and connections are uncommon.
            let remote_ip = conn.remote_ip();
            add_host(remote_ip, port);

            link_up(conn, SocketAddr::new(remote_ip, port));
            Ok(())
        }));

        Ok(())
    }).map_err(|err| {
        println!("error: {:?}", err);
    })
}

pub fn start() {
    let conf = state::get_conf();
    let args = state::get_args();

    tokio::run(futures::lazy(move || {
        // Listen
        println!("l={}", conf.port);
        tokio::spawn(listen(conf.port));

        // Terminal
        tokio::spawn(term::start());

        // Connect
        if let Some(rport) = args.remote_port {
            println!("c={}", rport);
            tokio::spawn(connect(rport));
        }

        Ok(())
    }));
}

