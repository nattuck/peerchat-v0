
use std::time::{Duration, SystemTime, UNIX_EPOCH};

pub fn d_to_ms(dd: Duration) -> u64 {
    dd.as_secs() * 1000 + dd.subsec_nanos() as u64 / 1_000_000
}

pub fn now_ms() -> u64 {
    let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
    d_to_ms(now)
}

