
use sodiumoxide::crypto::sign::PublicKey;
use sodiumoxide::crypto::sign;
use rmps;

use time;
use conf::Conf;
use host::Host;
use ::Result;

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum Msg {
    // Broadcast Messages
    Chat {
        sbod: Vec<u8>,
        pubk: PublicKey,
    },
    Host {
        host: Host,
    },

    // Direct Messages
    Hello {
        vers: u64,
        port: u16,
    },
    Cover {
        junk: Vec<u8>,
    },
    ReqHosts,
    Hosts {
        data: Vec<Host>,
    },
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct ChatBody {
    pub user: String,
    pub text: String,
    pub time: u64,
}

impl ChatBody {
    pub fn new<T: ToString, U: ToString>(text: T, user: U) -> Self {
        ChatBody {
            user: user.to_string(),
            text: text.to_string(),
            time: time::now_ms(),
        }
    }

    pub fn sign(&self, cfg: &Conf) -> Result<Msg> {
        let data = rmps::to_vec(self)?;
        let chat = Msg::Chat{
            pubk: cfg.pubk.clone(),
            sbod: sign::sign(&data, &cfg.seck),
        };
        Ok(chat)
    }

    pub fn verify(msg: &Msg) -> Result<Self> {
        match msg {
            Msg::Chat{ sbod, pubk }  => {
                let ver = sign::verify(&sbod, &pubk)
                    .map_err(|_| ::errors::Error::from("verify"))?;
                let body: ChatBody = rmps::from_slice(&ver)?;
                Ok(body)
            },
            _ => {
                Err("not a chat message".into())
            }
        }
    }
}

