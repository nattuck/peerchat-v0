
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};

use sodiumoxide::crypto::sign::PublicKey;

use errors::Result;
use peer::Peer;
use host::Host;
use user::User;
use conf::Conf;
use args::Args;

pub struct State {
    pub args:  Arc<Args>,
    pub conf:  Arc<Conf>,
    pub peers: Arc<Mutex<Vec<Peer>>>,
    pub hosts: Arc<Mutex<Vec<Host>>>,
    pub users: Arc<Mutex<Vec<User>>>,
}

impl State {
    fn default() -> Self {
        let args = Args::parse();

        State{
            args:  Arc::new(args.clone()),
            conf:  Arc::new(Conf::load(args).unwrap()),
            peers: Arc::new(Mutex::new(vec![])),
            hosts: Arc::new(Mutex::new(vec![])),
            users: Arc::new(Mutex::new(vec![])),
        }
    }
}

lazy_static! {
    static ref STATE: State = State::default();
}

pub fn get_peers() -> Arc<Mutex<Vec<Peer>>> {
    STATE.peers.clone()
}

pub fn dump_peers() {
    let pmx = get_peers();
    let peers = pmx.lock().unwrap();
    println!("\n== peers ==");
    for pp in &*peers {
        println!("{}", pp);
    }
}

pub fn put_peer(pp: Peer) {
    STATE.peers.lock().unwrap().push(pp);
}

pub fn get_hosts() -> Arc<Mutex<Vec<Host>>> {
    STATE.hosts.clone()
}

pub fn dump_hosts() {
    let hmx = get_hosts();
    let hosts = hmx.lock().unwrap();
    println!("\n== hosts ==");
    for hh in &*hosts {
        println!("{:?}", hh);
    }
}

pub fn put_host(host: Host) -> bool {
    let hmx = get_hosts();
    let mut hosts = hmx.lock().unwrap();

    for hh in &*hosts {
        if *hh == host {
            return false;
        }
    }

    hosts.push(host);
    return true;
}

pub fn find_host(addr: SocketAddr) -> Option<Host> {
    let hmx = get_hosts();
    let hosts = hmx.lock().unwrap();
    for hh in &*hosts {
        if hh.get_sock_addr() == addr {
            return Some(hh.clone())
        }
    }
    None
}

pub fn get_users() -> Arc<Mutex<Vec<User>>> {
    STATE.users.clone()
}

pub fn find_user_by_name(name: &str) -> Option<User> {
    let users = get_users();
    for uu in &*users.lock().unwrap() {
        if uu.name == name {
            return Some(uu.clone());
        }
    }
    None
}

pub fn find_user_by_pubk(pubk: &PublicKey) -> Option<User> {
    let users = get_users();
    for uu in &*users.lock().unwrap() {
        if uu.pubk == *pubk {
            return Some(uu.clone());
        }
    }
    None
}

pub fn put_user(name: &String, pubk: &PublicKey) -> Result<()> {
    let mut users = STATE.users.lock().unwrap();
    for uu in &*users {
        if uu.name == *name && uu.pubk == *pubk {
            return Ok(());
        }
        if uu.name == *name || uu.pubk == *pubk {
            return Err("duplicate user or key".into());
        }
    }

    users.push(User::new(name.clone(), pubk.clone(), 0));
    Ok(())
}

pub fn update_user_last(name: &String, time: u64) -> Result<u64> {
    let mut users = STATE.users.lock().unwrap();
    for mut uu in &mut *users {
        if uu.name == *name {
            let prev = uu.last;
            uu.last = time;
            return Ok(prev);
        }
    }

    Err("no such user".into())
}

pub fn get_args() -> Arc<Args> {
    STATE.args.clone()
}

pub fn get_conf() -> Arc<Conf> {
    STATE.conf.clone()
}

