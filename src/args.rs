
use std::env;
use std::process::exit;

use getopts::Options;

#[derive(Clone, Debug)]
pub struct Args {
    pub user: String,
    pub port: Option<u16>,
    pub remote_port: Option<u16>,
    pub pass: Option<String>,
}

impl Args {
    pub fn parse() -> Args {
        let mut ob = Options::new();
        ob.reqopt("u", "user", "user name", "NAME");
        ob.optopt("s", "pass", "password", "PASS");
        ob.optopt("p", "port", "listen port", "PORT");
        ob.optopt("c", "connect", "connect to remote port", "PORT");
        ob.optflag("h", "help", "show help message");

        let args: Vec<String> = env::args().collect();

        let opts = ob.parse(&args[1..]).map_err(|ee| {
            println!("Arg parse error: {:?}", ee);
            println!("{}", ob.usage(&args[0]));
            exit(1);
        }).unwrap();

        if opts.opt_present("h") {
            println!("{}", ob.usage(&args[0]));
            exit(0);
        }

        let user = opts.opt_str("u").unwrap();
        let port  = opts.opt_str("p")
            .map(|pp| pp.parse::<u16>().unwrap());
        let rport = opts.opt_str("c")
            .map(|rp| rp.parse::<u16>().unwrap());
        let pass = opts.opt_str("s");
        /*
        if opts.opt_present("c") {
            rport = Some(opts.opt_str("c").unwrap().parse::<u16>().unwrap());
        }
        */

        Args{
            user: user,
            pass: pass,
            port: port,
            remote_port: rport,
        }
    }
}
