
use std::net::{SocketAddr, IpAddr};
use std::str::FromStr;

#[derive(Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Host {
    addr: String,
    port: u16,
    seen: u64,
}

impl Host {
    pub fn new(addr: IpAddr, port: u16, seen: u64) -> Self {
        let mut host = Host{addr: "".into(), port: port, seen: seen};
        host.set_addr(addr);
        host
    }

    pub fn set_addr(&mut self, addr: IpAddr) {
        self.addr = format!("{}", addr);
    }

    pub fn set_addr_string(&mut self, text: String) {
        let addr = IpAddr::from_str(&text).unwrap();
        self.set_addr(addr);
    }

    pub fn get_addr(&self) -> Box<IpAddr> {
        Box::new(self.addr.parse().unwrap())
    }

    pub fn get_sock_addr(&self) -> SocketAddr {
        let ip = self.get_addr();
        SocketAddr::new(*ip, self.port)
    }

    pub fn get_seen(&self) -> u64 {
        self.seen
    }
}

