
#![recursion_limit = "4096"]

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate futures;
extern crate tokio;
extern crate bytes;
extern crate getopts;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;
extern crate toml;
extern crate termion;
#[macro_use]
extern crate lazy_static;
extern crate sodiumoxide;
extern crate xdg;

mod errors  {
    use std::error;
    use std::io;

    pub fn make_io<E: error::Error>(tag: &str, ee: E) -> io::Error {
        let text = format!("{}: {}", tag, ee.description());
        io::Error::new(io::ErrorKind::Other, text)
    }

    error_chain!{
        foreign_links {
            IO(::std::io::Error) #[cfg(unix)];
            TomlSer(::toml::ser::Error);
            TomlDe(::toml::de::Error);
            RmpsEn(::rmps::encode::Error);
            RmpsDe(::rmps::decode::Error);
            XDG(::xdg::BaseDirectoriesError);
        }
    }
}

pub use errors::*;

mod args;
mod conf;
mod time;
mod msg;
mod bvec_codec;
mod msg_codec;
mod term;
mod peer;
mod host;
mod user;
mod state;
mod comm;

fn main() -> Result<()> {
    sodiumoxide::init();
    comm::start();
    Ok(())
}

