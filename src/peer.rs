
use std::fmt;
use std::fmt::Display;
use std::sync::{Arc, Mutex};

use tokio;

use futures::Future;
use futures::sink::Sink;
use futures::sync::mpsc;

use msg::Msg;

pub struct Peer {
    adr: String,
    txq: mpsc::Sender<Msg>,
    done: Arc<Mutex<bool>>,
}

impl Peer {
    pub fn new(address: String, tx: mpsc::Sender<Msg>) -> Self {
        Peer{
            adr: address,
            txq: tx,
            done: Arc::new(Mutex::new(false)),
        }
    }

    pub fn send(&mut self, mm: Msg) {
        //println!(".");

        let darc = self.done.clone();

        let ff = self.txq.clone().send(mm.clone()).
            map(|_| {}).
            map_err(move |ee| {
                //println!("send error: {:?}", ee.description());
                let mut done = darc.lock().unwrap();
                *done = true;
            });
        tokio::spawn(ff);
    }

    pub fn alive(&self) -> bool {
        !*self.done.lock().unwrap()
    }
}

impl Display for Peer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "peer: {}", self.adr)
    }
}

