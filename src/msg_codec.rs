use std::error::Error;
use std::net::IpAddr;

use tokio::prelude::*;
use tokio::net::TcpStream;
use tokio::io;
use rmps;
use sodiumoxide::crypto::secretbox;

use msg::Msg;
use bvec_codec::BvecCodec;
use state;

pub struct MsgCodec {
    conn: BvecCodec,
}

impl MsgCodec {
    pub fn new(sock: TcpStream) -> Self {
        MsgCodec{
            conn: BvecCodec::new(sock),
        }
    }

    pub fn remote_ip(&self) -> IpAddr {
        self.conn.remote_ip()
    }
}

impl Stream for MsgCodec {
    type Item = Msg;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        match try_ready!(self.conn.poll()) {
            Some(data) => {
                let conf = state::get_conf();
                let nonce = secretbox::Nonce::from_slice(&data[0..24]).unwrap();
                let ctext = &data[24..];
                let ptxt = secretbox::open(&ctext, &nonce, &conf.boxk).map_err(|_| {
                    io::Error::new(io::ErrorKind::Other, "message unbox failed")
                })?;
                let msg = rmps::from_slice(&ptxt[..]).map_err(|ee| {
                    let text = format!("MsgCodec#poll: {}", ee.description());
                    io::Error::new(io::ErrorKind::Other, text)
                })?;
                Ok(Async::Ready(Some(msg)))
            },
            None => Ok(Async::Ready(None)),
        }
    }
}

impl Sink for MsgCodec {
    type SinkItem = Msg;
    type SinkError = io::Error;

    fn start_send(&mut self, item: Self::SinkItem)
      -> Result<AsyncSink<Self::SinkItem>, Self::SinkError>
    {
        let ptxt = rmps::to_vec(&item).map_err(|ee| {
            let text = format!("MsgCodec#start_send: {}", ee.description());
            io::Error::new(io::ErrorKind::Other, text)
        })?;

        let conf = state::get_conf();
        let nonce = secretbox::gen_nonce();
        let ctxt = secretbox::seal(&ptxt, &nonce, &conf.boxk);

        let mut data = vec![];
        data.extend_from_slice(nonce.as_ref());
        data.extend_from_slice(&ctxt);

        self.conn.start_send(data).map(move |aa| {
            aa.map(|_| item)
        })
    }

    fn poll_complete(&mut self) -> Poll<(), Self::SinkError>
    {
        self.conn.poll_complete()
    }
}

