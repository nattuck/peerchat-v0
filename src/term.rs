
use std::process;
use std::error::Error;
use std::io::BufReader;

use futures::Future;
use tokio::prelude::*;
use tokio::io;
use tokio;

use msg::ChatBody;
use comm::got_message;
use state;

pub fn putln<S: AsRef<str>>(text: S) {
    println!("\r{}", text.as_ref());
    print!("> ");
    io::stdout().flush().unwrap();
}

pub fn start() -> impl Future<Item=(), Error=()> + Send {
    future::lazy(|| {
        putln("startup");
        let read_lines = io::lines(BufReader::new(io::stdin()))
            .for_each(|line| {
                let line = line.trim();

                if line.chars().nth(0) == Some('/') {
                    let cmd = &line[1..];
                    putln(format!("cmd: {}", cmd));
                    run_command(cmd);
                    return Ok(());
                }

                let cfg = state::get_conf();
                let msg = ChatBody::new(line, cfg.user.clone())
                    .sign(&cfg).map_err(|ee| ::errors::make_io("sign", ee))?;
                got_message(msg).map_err(|ee| ::errors::make_io("got", ee))?;
                Ok(())
            })
            .map_err(|ee| {
                println!("read err: {}", ee.description());
            });
        tokio::spawn(read_lines);
        Ok(())
    })
}

fn run_command(cmd: &str) {
    match cmd {
        "quit" => {
            println!("\nexit requested.");
            process::exit(0);
        },
        "peers" => {
            state::dump_peers();
            putln("");
        },
        "hosts" => {
            state::dump_hosts();
            putln("");
        },
        other => {
            println!("\nunknown cmd: '{}'", other);
        }
    }
}
