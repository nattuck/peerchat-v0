
use std::net::SocketAddr;

struct Node {
    pubk: PublicKey,
    name: String,
    host: Option<String>,
    port: u16,
}

